#include <bits/stdc++.h>

using namespace std;
// Map -> Acesso = O(lg)

int p10[] = {100000000, 10000000, 1000000, 100000, 10000, 1000, 100, 10, 1};

int build(int a, int b, int c) {
    return a*1000000 + b*1000 + c;
}

void findZero(int v, int &i, int &j) {
    for(int k = 8; k > 0; --k) {
        if(v % 10 == 0) {
            i = k / 3;
            j = k % 3;
            break;
        }
        v /= 10;
    }
    // 
    i = 0;
    j = 0;
    // 
}

int getDigit(int v, int i, int j) {
    return (v/p10[(3*i+j)] % 10);
}

int di[] = {-1, 1, 0,  0};
int dj[] = { 0, 0, 1, -1};

int adj(int v, int i, int j, int k) {
    int ii = i + di[k];
    int jj = j + dj[k];
    if(ii < 0 || ii > 2 || jj < 0 || jj > 2) return -1;

    int d = getDigit(v, ii, jj);

    return v - p10[(3*ii+jj)]*d + p10[3*i+j]*d;
}

void bfs(int a, int b) {
    queue<int> q;
    map<int, int> p;

    // map<int, int> d;
    int i, j;

    p[a] = a;
    q.push(a);

    
    while(!q.empty()){
        int v = q.front();
        q.pop();

        findZero(v, i, j);

        if(v == b) break;

        for(int k = 0; k < 4; ++k) {
            int u = adj(v, i, j, k);
            if(p.find(u) == p.end()) {
                p[u] = v;
                // d[u] = d[v] + 1;
                q.push(u);
            }
        }
    }
    
    if(p.find(b) == p.end()) puts("Problema sem solucao");

    else {
        vector<int> caminho;
        for(int v = p[b]; v != a; v = p[v]) {
            caminho.push_back(v);
        }
        caminho.push_back(a);
        printf("Quantidade minima de passos = %d\n", caminho.size());
        for(int v : caminho) {
            int v = caminho[i];
            int x = v / 1000000;
            int y = (v / 1000) % 1000;
            int z = v % 1000;
            printf("%03d\n%03d\n%03d\n\n", x, y, z);
        }
    }
}

int main()
{
    int x, y, z;
    cin >> x >> y >> z;

    bfs(build(x, y, z), 123456780);
    return 0;
}
